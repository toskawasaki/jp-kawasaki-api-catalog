# Panasonic Digital Platform Portal

A statically generated dat catalogue.

This is the codebase to build it.

Pretty easy.

```
npm install
npm run build
```

For development:

```
npm run dev
```

This is all built on [gatsbyjs](//github.com/gatsbyjs/gatsby).
