'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var onCreateNode = function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(_ref2) {
    var node = _ref2.node,
        getNode = _ref2.getNode,
        boundActionCreators = _ref2.boundActionCreators,
        loadNodeContent = _ref2.loadNodeContent;
    var createNode, createParentChildLink, content, data, dataStr, parsedSpec, contentDigest, swaggerNode, parsedFilePath, slug;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            createNode = boundActionCreators.createNode, createParentChildLink = boundActionCreators.createParentChildLink;

            // Don't reprocess our own nodes!  (note: this doesn't normally happen
            // but since this transformer creates new nodes with the same media-type
            // as its parent node, we have to add this check that we didn't create
            // the node).

            if (!(node.internal.owner === 'gatsby-transformer-swagger')) {
              _context.next = 3;
              break;
            }

            return _context.abrupt('return');

          case 3:
            if (!(node.internal.mediaType !== 'text/yaml')) {
              _context.next = 5;
              break;
            }

            return _context.abrupt('return');

          case 5:
            _context.next = 7;
            return loadNodeContent(node);

          case 7:
            content = _context.sent;
            data = jsYaml.load(content);
            dataStr = (0, _stringify2.default)(data);
            // for fun, lets dump the whole swagger spec onto the graphql node. We
            // need to mangle the responses object a little because numbers cannot be
            // keys in graphql. We're going to use JSON.parse here because we've got JSON
            // and it's too late in the evening for me to think of a better way. Soz!

            parsedSpec = JSON.parse(dataStr, function (key, value) {
              if (key === 'responses') {
                var responses = (0, _assign2.default)({}, value);
                for (var prop in responses) {
                  if (prop !== 'default') {
                    responses['http_' + prop] = responses[prop];
                    //Object.defineProperty(responses, `http_${prop}`, Object.getOwnPropertyDescriptor(responses, prop))
                    delete responses[prop];
                  }
                }
                return responses;
              }
              return value;
            });
            contentDigest = crypto.createHash('md5').update(dataStr).digest('hex');
            swaggerNode = {
              id: node.id + ' >>> Swagger',
              children: [],
              parent: node.id,
              info: parsedSpec.info,
              internal: {
                contentDigest: contentDigest,
                type: 'Swagger',
                mediaType: 'text/yaml',
                content: dataStr
              },
              spec: (0, _extends3.default)({}, parsedSpec),
              swagger: dataStr
            };
            parsedFilePath = path.parse(node.relativePath);
            slug = '/' + parsedFilePath.dir + '/';

            if ((0, _typeof3.default)(data.info) !== undefined && (0, _typeof3.default)(data.info['x-catalogue']) !== undefined) {
              swaggerNode.catalogue = data.info['x-catalogue'];
              //slug = `/${swaggerNode.catalogue.category}/${parsedFilePath.dir}/`
            }
            swaggerNode.slug = slug;

            // Add path to the swagger file path
            if (node.internal.type === 'File') {
              swaggerNode.fileAbsolutePath = node.absolutePath;
            }

            createNode(swaggerNode);
            createParentChildLink({ parent: node, child: swaggerNode });

            return _context.abrupt('return');

          case 21:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function onCreateNode(_x) {
    return _ref.apply(this, arguments);
  };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Promise = require('bluebird');
var fs = require('fs');
var jsYaml = require('js-yaml');
var _ = require('lodash');
var crypto = require('crypto');
var path = require('path');

exports.onCreateNode = onCreateNode;