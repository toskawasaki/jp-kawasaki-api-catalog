const Promise = require(`bluebird`)
const fs = require(`fs`)
const jsYaml = require(`js-yaml`)
const _ = require(`lodash`)
const crypto = require(`crypto`)
const path = require('path')

async function onCreateNode({ node, getNode, boundActionCreators, loadNodeContent }) {
  const { createNode, createParentChildLink } = boundActionCreators

  // Don't reprocess our own nodes!  (note: this doesn't normally happen
  // but since this transformer creates new nodes with the same media-type
  // as its parent node, we have to add this check that we didn't create
  // the node).
  if (node.internal.owner === `gatsby-transformer-swagger`) {
    return
  }

  // @todo: Make this check it's actually a swagger yaml file. Also, add json?
  if (node.internal.mediaType !== `text/yaml`) {
    return
  }

  const content = await loadNodeContent(node)
  const data = jsYaml.load(content)
  const dataStr = JSON.stringify(data)
  // for fun, lets dump the whole swagger spec onto the graphql node. We
  // need to mangle the responses object a little because numbers cannot be
  // keys in graphql. We're going to use JSON.parse here because we've got JSON
  // and it's too late in the evening for me to think of a better way. Soz!
  const parsedSpec = JSON.parse(dataStr, (key, value) => {
    if ( key === `responses` ) {
      const responses = Object.assign({}, value)
      for (let prop in responses) {
        if (prop !== `default`) {
          responses[`http_${prop}`] = responses[prop]
          //Object.defineProperty(responses, `http_${prop}`, Object.getOwnPropertyDescriptor(responses, prop))
          delete responses[prop]
        }
      }
      return responses
    }
    return value
  })

  const contentDigest = crypto
    .createHash(`md5`)
    .update(dataStr)
    .digest(`hex`)

  const swaggerNode = {
    id: `${node.id} >>> Swagger`,
    children: [],
    parent: node.id,
    info: parsedSpec.info,
    internal: {
      contentDigest,
      type: `Swagger`,
      mediaType: `text/yaml`,
      content: dataStr,
    },
    spec: { ...parsedSpec },
    swagger: dataStr,
  }
  const parsedFilePath = path.parse(node.relativePath)
  let slug = `/${parsedFilePath.dir}/`
  if(typeof data.info !== undefined && typeof data.info['x-catalogue'] !== undefined) {
    swaggerNode.catalogue = data.info['x-catalogue']
    //slug = `/${swaggerNode.catalogue.category}/${parsedFilePath.dir}/`
  }
  swaggerNode.slug = slug

  // Add path to the swagger file path
  if (node.internal.type === `File`) {
    swaggerNode.fileAbsolutePath = node.absolutePath
  }

  createNode(swaggerNode)
  createParentChildLink({ parent: node, child: swaggerNode })

  return
}

exports.onCreateNode = onCreateNode
