const _ = require('lodash')
const Promise = require('bluebird')
const path = require('path')
const select = require(`unist-util-select`)
const precache = require(`sw-precache`)
const fs = require(`fs-extra`)


exports.modifyWebpackConfig = ({config, stage}) => {
    config.target = 'node'

    config.loader(`@epegzz/sass-vars-loader`, {
        test: /\.scss$/,
        loader: `@epegzz/sass-vars-loader`,
        query: {
            files: [
                path.resolve(__dirname, 'config-colors.json'),
            ],
        },
    })

    return config
}


exports.createPages = ({graphql, boundActionCreators}) => {
    const {createPage} = boundActionCreators

    return new Promise((resolve, reject) => {
        const pages = []
        const templates = {
            default: path.resolve('./src/templates/template-default.js'),
            howto: path.resolve('./src/templates/template-howto.js'),
            api: path.resolve('./src/templates/template-api.js')
        }

        graphql(
            `
              {
                allMarkdownRemark(limit: 1000) {
                  edges {
                    node {
                      fields {
                        slug
                      }
                      frontmatter {
                        template
                      }
                    }
                  }
                }
                allSwagger {
                 edges {
                   node {
                     id
                     internal {
                       owner
                     }
                     catalogue {
                       category
                     }
                     spec {
                       swagger
                       host
                       basePath
                       info {
                         title
                         description
                         version
                       }
                       schemes
                     }
                   }
                 }
                }
              }
            `
        ).then(result => {
            if (result.errors) {
                resolve()
                // reject(result.errors);
            }

            // Create markdown posts pages.
            _.each(result.data.allMarkdownRemark.edges, edge => {
                createPage({
                    path: edge.node.fields.slug, // required
                    component: templates[edge.node.frontmatter.template] ? templates[edge.node.frontmatter.template] : templates.default,
                    context: {
                        slug: edge.node.fields.slug,
                    },
                })
            })

            resolve()
        })

        const swaggerTemplate = path.resolve('./src/templates/template-api.js')

        graphql(
            `
              {
                allSwagger {
                  edges {
                    node {
                      slug
                    }
                  }
                }
              }
            `
        ).then(result => {
            if (result.errors) {
                resolve()
                // reject(result.errors);
            }

            // Create beta posts pages.
            _.each(result.data.allSwagger.edges, edge => {
                createPage({
                    path: edge.node.slug, // required
                    component: swaggerTemplate,
                    context: {
                        slug: edge.node.slug,
                    },
                })
            })

            resolve()
        })
    })
}

// Add a route match for the /portal path to enable client-only paths
// all paths under /portal/ are routed to the account page, and from there we do magic!
exports.onCreatePage = ({page, boundActionCreators}) => {
    const {createPage} = boundActionCreators
    return new Promise((resolve, reject) => {
        if (page.path === `/account/`) {
            page.matchPath = `/account/:path` //eslint-disable-line
            createPage(page)
        }
        resolve()
    })
}

// Add custom url pathname and hero image for blog posts.
exports.onCreateNode = ({node, boundActionCreators, getNode}) => {
    const {createNodeField} = boundActionCreators
    if (node.internal.type === `File`) {
        const parsedFilePath = path.parse(node.relativePath)
        const slug = `/${parsedFilePath.dir}/`
        createNodeField({node, name: `slug`, value: slug})
    } else if (
        node.internal.type === `MarkdownRemark` &&
        typeof node.slug === `undefined`
    ) {
        const fileNode = getNode(node.parent)
        createNodeField({
            node,
            name: `slug`,
            value: fileNode.fields.slug,
        })
    }
}
