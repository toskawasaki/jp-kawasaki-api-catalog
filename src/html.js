import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TypographyStyle, GoogleFont } from 'react-typography'
import Helmet from 'react-helmet'

import typography from './assets/scripts/typography'

let stylesStr
if (process.env.NODE_ENV === `production`) {
  try {
    stylesStr = require(`!raw-loader!../public/styles.css`)
  } catch (e) {
    console.log(e)
  }
}

class Html extends Component {
  render() {
    const head = Helmet.rewind()
    let css
    if (process.env.NODE_ENV === `production`) {
      css = <style id="gatsby-inlined-css" dangerouslySetInnerHTML={{ __html: stylesStr }} />
    }
      
    return (
      <html lang="en">
        <head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta name="robots" content="noindex" />

          <link rel="shortcut icon" href={require('../static/favicon.ico')} />
          <link rel="apple-touch-icon" href={require('../static/apple-touch-icon.png')} />
          <link rel="msapplication-TileImage" href={require('../static/metro-tile.png')} />

          {this.props.headComponents}
          <TypographyStyle typography={typography} />
          <GoogleFont typography={typography} />
          {css}
        </head>
        <body>
          {this.props.preBodyComponents}
          <div id="___gatsby" dangerouslySetInnerHTML={{ __html: this.props.body }} />
          {this.props.postBodyComponents}
        </body>
      </html>
    )
  }
}

Html.propTypes = {
  headComponents: PropTypes.array.isRequired,
  body: PropTypes.string.isRequired,
  preBodyComponents: PropTypes.array,
  postBodyComponents: PropTypes.array,
}

module.exports = Html
