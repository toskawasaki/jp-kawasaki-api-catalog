import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Svg from '../../components/Svg'

const config = require('../../../config-theme.json')

class Header extends Component {
  render() {
      const { title, link } = this.props

    return (
      <header id="top" className="global-header">
          <div className='l-padding'>
              <div className='logo'>
                  <Link to={link} style={{width:420, height:80}}>
                      <Svg src={require('../../../static/panasonic-logo.svg')} style={{width:420, height:80}} alt={config.theme.title} />
                  </Link>
              </div>
              <h1 dangerouslySetInnerHTML={{ __html: title}} />
          </div>
      </header>
    )
  }
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
}

export default Header