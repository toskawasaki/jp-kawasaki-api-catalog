import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class SideMenu extends Component {

    render() {
        const { title } = this.props

        return (

            <aside id="aside" tabIndex="-1" role="complementary" className="l-aside">
                <nav className="nav-aside" role="navigation">
                    <h6>{title}</h6>
                    {this.props.children}
                </nav>
            </aside>

        )
    }
}

SideMenu.propTypes = {
    title: PropTypes.string,
}

export default SideMenu
