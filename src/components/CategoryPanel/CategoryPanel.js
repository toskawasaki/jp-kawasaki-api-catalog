import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Svg from '../../components/Svg'

import HelperClass from '../../utils/helper'

class CategoryPanel extends Component {

    render() {
        const { title, link, icon } = this.props

        return (
            <li className='sl-item'>
                <Link to={link} className='api-tile'>
                    {/*<Svg src={icon} style={{width:50, height:50}} alt={title} />*/}
                    <h3 className={HelperClass.setIconClass('icon', icon)}>{title}</h3>
                </Link>
            </li>
        )
    }
}

CategoryPanel.propTypes = {
    title: PropTypes.string,
    link: PropTypes.string,
    icon: PropTypes.string,
}

export default CategoryPanel
