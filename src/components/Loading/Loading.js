import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Svg from '../../components/Svg'

class Loading extends Component {

    render() {

        return (

            <section className={this.props.active ? 'loading active': 'loading'}>
                {this.props.children}
                <Svg src={require('../../../static/loading-icon-blue.svg')} style={{width:40, height:40}} alt='Loading...' />
            </section>

        )
    }
}

Loading.propTypes = {
    active: PropTypes.bool.isRequired,
    children: PropTypes.object.isRequired,
}

export default Loading
