import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text } from 'react-form'
import { Link } from 'react-router-dom'

class RegisterForm extends Component {

    render() {

        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                validate={values => {
                    const {name, email, password, confirm} = values
                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    const uppercase = /^(?=.*[A-Z])/
                    const lowercase = /^(?=.*[a-z])/
                    const digits = /^(?=.*\d)/
                    const special = /^(?=.*[_\W])/
                    let pwd = null

                    if (!password) {
                        pwd = 'Password is required'
                    } else if (password && !lowercase.test(password)) {
                        pwd = 'Password must contain lowercase characters'
                    } else if (password && !uppercase.test(password)) {
                        pwd = 'Password must contain uppercase characters'
                    } else if (password && !digits.test(password)) {
                        pwd = 'Password must contain numbers'
                    } else if (password && !special.test(password)) {
                        pwd = 'Password must contain special characters'
                    } else if (password && password.length < 8) {
                        pwd = 'Password must have a length of 8 characters or greater'
                    }

                    return {
                        name: !name ? 'Your name is required' : undefined,
                        email: !re.test(email) ? 'Email address is required' : undefined,
                        password: pwd,
                        confirm: (!confirm || confirm != password) ? 'Confirm password must match the password entered above' : undefined,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='register' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Register</legend>
                                <div className="ctrl-holder width-l">
                                    <label>Your name<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='name' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Email<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='email' className="text" type="email" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Password<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='password' className="text" type="password" />
                                        <p>Your password must contain:</p>
                                        <ul>
                                            <li>Requires uppercase letter</li>
                                            <li>Requires lowercase letter</li>
                                            <li>Requires number</li>
                                            <li>Requires special character</li>
                                            <li>Requires minimum length of 8 characters</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Confirm password<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='confirm' className="text" type="password" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <p>By <b>Registering</b> you agree that you have read and accepted our data use policy from our <a href="/copyright" target="_blank">Copyright</a> and also our <a href="/privacy" target="_blank">Privacy Policy</a>.</p>
                                        <button className="cta" type='submit'>Register an account</button>
                                        <Link to='/account/' className="cta is-secondary">Cancel</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default RegisterForm
