import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

import MenuItem from '../../components/MenuItem'

class FooterNav extends Component {
    render() {

        const linksList = []
        this.props.children.forEach((link, index) => {
            linksList.push(
                <MenuItem key={index} title={link.title} link={link.link} isExternal={link.external} />
            )
        })

        return (

            <nav>
                <ul className="nav-footer">

                    {linksList}

                </ul>
            </nav>

        )
    }
}

FooterNav.propTypes = {
    children: PropTypes.array.isRequired,
}

export default FooterNav
