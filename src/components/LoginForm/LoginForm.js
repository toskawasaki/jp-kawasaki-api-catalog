import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text, FormError } from 'react-form'
import { Link } from 'react-router-dom'

class LoginForm extends Component {

    render() {
        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                validate={values => {
                    const { email, password } = values
                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    return {
                        email: !re.test(email) ? 'Email address is required' : undefined,
                        password: !password ? 'Password is required' : undefined,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='sign-in' className="login-form" onSubmit={submitForm}>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <label>Email<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='email' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Password<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='password' className="text" type='password' />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <button className="cta" type='submit'>Sign in</button>
                                        <Link to='/account/register' className="cta is-secondary">Register an account</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default LoginForm
