import React, { Component } from 'react'
import PropTypes from 'prop-types'

import HelperClass from '../../utils/helper'

class PageHeader extends Component {

    render() {
        const { title, category } = this.props

        return (

            <header className="content-header">
                <h1 className={HelperClass.setIconClass('icon', category)}>{title}</h1>
            </header>

        )
    }
}

PageHeader.propTypes = {
    title: PropTypes.string,
    category: PropTypes.string,
}

export default PageHeader
