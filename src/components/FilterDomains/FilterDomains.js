import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form } from 'react-form'

import CheckboxField from '../../components/Checkbox'

class FilterDomains extends Component {

    render() {

        return (
            <Form
                onChange={this.props.onChange}
            >
                {({submitForm}) => {

                    const categoryList = []
                    this.props.children.forEach((category, index) => {
                        categoryList.push(
                            <CheckboxField key={index} title={category} event={submitForm} />
                        )
                    })

                    return (
                        <form id='filter-domains' className="filter-form">
                            <legend>Filter Domain</legend>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    {categoryList}
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

FilterDomains.propTypes = {
    children: PropTypes.array,
}

export default FilterDomains
