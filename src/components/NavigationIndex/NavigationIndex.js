import React, { Component } from 'react'
import PropTypes from 'prop-types'

import MenuItem from '../../components/MenuItem'

class NavigationIndex extends Component {
    constructor() {
        super()
        this.state = {
            expanded: false,
        }
    }
    render() {

        const linksList = []
        this.props.children.forEach((link, index) => {
            linksList.push(
                <MenuItem key={index} title={link.title} link={link.link} isExternal={link.external} />
            )
        })

        return (

            <section>
                <a href="javascript:void(0)" className={this.state.expanded ? 'burger expanded': 'burger'} onClick={() => this.setState({ expanded: !this.state.expanded })}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <nav className={this.state.expanded ? 'nav-top is-home expanded': 'nav-top is-home'} role="navigation">
                    <div className="l-padding">

                        <ul className="main">

                            {linksList}

                        </ul>

                    </div>
                </nav>
            </section>

        )
    }
}

NavigationIndex.propTypes = {
  children: PropTypes.array.isRequired,
}

export default NavigationIndex
