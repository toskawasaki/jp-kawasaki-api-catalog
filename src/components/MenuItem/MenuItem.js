import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import { withRouter } from 'react-router-dom'

@withRouter
class MenuItem extends Component {
  render() {
    const { title, link, isExternal } = this.props

    return (
        <li>
            <Link to={link} target={isExternal ? '_blank' : '_self'}>{title}</Link>
        </li>
    )
  }
}

MenuItem.propTypes = {
    title: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    isExternal: PropTypes.bool
}

export default MenuItem
