import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'

class Card extends Component {

    render() {
        const { slug, title, type, blurb, status } = this.props

        return (
            <li className="sl-item">
                <Link to={slug} className="api-tile">
                    <h3 className="link-caret"><span>{title}</span></h3>
                    <h4>Data Type: {type}</h4>
                    <p>{blurb}</p>
                    <p className="status">{status}</p>
                </Link>
            </li>
        )
    }
}

Card.propTypes = {
    slug: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    blurb: PropTypes.string,
    status: PropTypes.string,
}

export default Card
