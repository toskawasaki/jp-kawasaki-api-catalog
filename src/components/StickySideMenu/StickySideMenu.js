import React, { Component } from 'react'
import { Sticky } from 'react-sticky'

class StickySideMenu extends Component {

    render() {

        return (

            <div className="sticky-wrapper">
                <Sticky>
                    {
                        ({ style }) => {

                            // overwrite the default sticky styles
                            style.top = 60
                            style.width = '250px'

                            return (
                                <div className="sidemenu" style={style}>
                                    {this.props.children}
                                </div>
                            )
                        }
                    }

                </Sticky>
            </div>

        )
    }
}

export default StickySideMenu
