import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Svg from '../../components/Svg'

const config = require('../../../config-theme.json')

class HeaderHome extends Component {
  render() {
      const { title, slogan, link } = this.props

    return (
      <header id="top" className="global-header is-home">
          <div className='l-padding'>
              <div className='logo'>
                  <Link to={link} style={{width:420, height:80}}>
                      <Svg src={require('../../../static/panasonic-logo.svg')} style={{width:420, height:80}} alt={config.theme.title} />
                  </Link>
              </div>
              <div className="header-content">
                  <h1 dangerouslySetInnerHTML={{ __html: title}} />
                  <p>{slogan}</p>
              </div>
          </div>
      </header>
    )
  }
}

HeaderHome.propTypes = {
    title: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    slogan: PropTypes.string,
}

export default HeaderHome

