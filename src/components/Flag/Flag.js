import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Flag extends Component {

    render() {

        return (

            <section className="flag">{this.props.text}</section>

        )
    }
}

Flag.propTypes = {
    text: PropTypes.string.isRequired,
}

export default Flag
