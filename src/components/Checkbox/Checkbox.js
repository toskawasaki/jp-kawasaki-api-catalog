import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Checkbox } from 'react-form'

class CheckboxField extends Component {

    constructor() {
        super()
        this.state = {
            checked: false,
        }
    }

    render() {
        const { title } = this.props

        return (
            <label>
                <Checkbox field={title} onChange={(e, onChange) => {onChange(this.props.event)}}/>
                <span className={this.state.checked ? 'checked' : ''} onClick={() => this.setState({checked: !this.state.checked})}>{title}</span>
            </label>
        )
    }
}

CheckboxField.propTypes = {
    title: PropTypes.string.isRequired,
}

export default CheckboxField
