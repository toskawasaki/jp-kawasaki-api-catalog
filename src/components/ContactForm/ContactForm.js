import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text, Textarea, FormError } from 'react-form'

class ContactForm extends Component {

    render() {

        return (
            <Form
                onSubmit={(values) => {
                    // console.log('Success!', values)
                }}

                validate={values => {
                    const { name, email, enquiry } = values
                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    return {
                        name: !name ? 'Your name is required' : undefined,
                        email: !re.test(email) ? 'Email address is required' : undefined,
                        enquiry: !enquiry ? 'Enquiry message is required' : undefined,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='contact-us' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Contact us</legend>
                                <div className="ctrl-holder width-xl">
                                    <label>Please provide a description of your enquiry or issue<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Textarea field="enquiry" className="text" />
                                    </div>
                                </div>

                                <div className="ctrl-holder width-xl">
                                    <label>Your contact details</label>
                                    <div className="ctrl">
                                        <p>We ask that you provide your contact details in case we need to clarify the information or ask for more details. We will only contact you if we need further clarification.</p>
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Your name<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='name' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Email<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='email' className="text" type="email" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Contact number</label>
                                    <div className="ctrl">
                                        <Text field='number' className="text" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <button className="cta" type='submit'>Submit</button>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <h3>What happens next</h3>
                                        <p>Your feedback will be used to improve our information, products and services.</p>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default ContactForm
