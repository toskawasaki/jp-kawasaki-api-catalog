import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text } from 'react-form'
import { Link } from 'react-router-dom'

class ResetPasswordForm extends Component {

    render() {

        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                validate={values => {
                    const { email, code, password } = values
                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    const uppercase = /^(?=.*[A-Z])/
                    const lowercase = /^(?=.*[a-z])/
                    const digits = /^(?=.*\d)/
                    const special = /^(?=.*[_\W])/
                    let pwd = null

                    if (!password) {
                        pwd = 'Password is required'
                    } else if (password && !lowercase.test(password)) {
                        pwd = 'Password must contain lowercase characters'
                    } else if (password && !uppercase.test(password)) {
                        pwd = 'Password must contain uppercase characters'
                    } else if (password && !digits.test(password)) {
                        pwd = 'Password must contain numbers'
                    } else if (password && !special.test(password)) {
                        pwd = 'Password must contain special characters'
                    } else if (password && password.length < 8) {
                        pwd = 'Password must have a length of 8 characters or greater'
                    }

                    return {
                        email: !re.test(email) ? 'Email address is required' : undefined,
                        code: !code ? 'The unique code from the email is required' : undefined,
                        password: pwd,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='sign-in' className="login-form" onSubmit={submitForm}>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <label>Email<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='email' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Verification code<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='code' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>New Password<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='password' className='text' type='password'/>
                                        <p>Your password must contain:</p>
                                        <ul>
                                            <li>Requires uppercase letter</li>
                                            <li>Requires lowercase letter</li>
                                            <li>Requires number</li>
                                            <li>Requires special character</li>
                                            <li>Requires minimum length of 8 characters</li>
                                        </ul>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <button className="cta" type='submit'>Submit</button>
                                        {/*<Link to='/app' className="cta is-secondary">Cancel</Link>*/}
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default ResetPasswordForm
