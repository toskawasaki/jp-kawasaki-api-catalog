import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text } from 'react-form'
import CopyToClipboard from 'react-copy-to-clipboard'

class Key extends Component {

    constructor() {
        super()
        this.state = {
            copied: false,
        }
    }

    render() {

        const { apiKey, apiID, apiTitle, apiDescription } = this.props

        return (

            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                defaultValues={{
                    apiID: apiID
                }}
            >
                {({submitForm}) => {
                    return (
                        <section className="api-key">

                            <h2 className="has-border">{this.props.apiTitle}</h2>
                            <p>{this.props.apiDescription}</p>

                            <div className="key-block">

                                <CopyToClipboard text={this.props.apiKey} onCopy={() => this.setState({copied: true})}>
                                    <button>Copy API key</button>
                                </CopyToClipboard>

                                <input value={this.props.apiKey} readOnly='readOnly' />

                                {this.state.copied ? <div className='checkmark draw' title="Copied to clipboard"></div> : null}

                                <form id='remove-api-key' onSubmit={submitForm}>
                                    <fieldset>
                                        <Text field='apiID' className="text" type="hidden" />
                                        <button className="cta is-secondary" type='submit'>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M21 6l-3 18h-12l-3-18h2.028l2.666 16h8.611l2.666-16h2.029zm-4.711-4c-.9 0-1.631-1.099-1.631-2h-5.316c0 .901-.73 2-1.631 2h-5.711v2h20v-2h-5.711z"/></svg>
                                        </button>
                                    </fieldset>
                                </form>

                            </div>

                        </section>
                    )
                }}
            </Form>

        )

    }
}

Key.propTypes = {
    apiKey: PropTypes.string,
    apiID: PropTypes.string,
    apiTitle: PropTypes.string,
    apiDescription: PropTypes.string,
}

export default Key
