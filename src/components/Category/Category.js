import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import Card from '../../components/Card'

import HelperClass from '../../utils/helper'

class CategoryBlock extends Component {

  generateAPICards(apis) {
    const items = []
      if(apis.length > 0){
          apis.forEach((api) => {
              items.push(
                  <Card
                    key={api.node.slug}
                    slug={api.node.slug}
                    title={api.node.spec.info.title}
                    type={api.node.catalogue.type}
                    blurb={api.node.spec.info.description}
                    status={api.node.catalogue.status}
                  />
              )
          })
      }

      return items
  }

  render() {
    const { nodes, title } = this.props
    const catSlug = title

    return (
      <div id={title} className="sl bom-catalogue-category">
        <h2 className={HelperClass.setIconClass('icon', catSlug)}>{title}</h2>
        <ul role="category-items">
          {this.generateAPICards(nodes)}
        </ul>
      </div>
    )
  }
}

export default CategoryBlock
