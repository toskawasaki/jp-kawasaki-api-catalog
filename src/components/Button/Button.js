import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class Button extends Component {

    render() {
        const { text, link, isSecondary } = this.props

        function setClasses() {
            let classes = 'cta'
            isSecondary ? classes = classes.concat(' is-secondary') : null
            return classes
        }

        return (

            <Link to={link} className={setClasses()}>{text}</Link>

        )
    }
}

Button.propTypes = {
    text: PropTypes.string,
    link: PropTypes.string,
    isSecondary: PropTypes.bool,
}

export default Button
