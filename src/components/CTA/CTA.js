import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class CTA extends Component {

    render() {
        const { title, text, buttonLink, buttonText, hasBorder, isCentered} = this.props

        function setClasses() {
            let classes = 'cta-block'
            hasBorder ? classes = classes.concat(' has-border') : null
            isCentered ? classes = classes.concat(' is-centered') : null
            return classes
        }

        function hasButton(includeButton) {
            if (includeButton) {
                return <Link to={buttonLink} className="cta">{buttonText}</Link>
            }
            return null
        }

        return (
            <div className={setClasses()}>
                <div className="wrap">
                    <h2>{title}</h2>
                    <p>{text}</p>
                    {hasButton(buttonText)}
                </div>
            </div>
        )
    }
}

CTA.propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    buttonText: PropTypes.string,
    buttonLink: PropTypes.string,
    hasBorder: PropTypes.bool,
    isCentered: PropTypes.bool,
}

export default CTA
