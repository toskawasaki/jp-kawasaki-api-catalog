import React, { Component } from 'react'
import { Redirect, Route } from 'react-router'
import { inject } from 'mobx-react'

@inject('userStore')
class PrivateRoute extends Component {
  render() {
    const Component = this.props.component

    return (
      <Route render={() => (
        this.props.userStore.isLoggedIn ? (
          <Component {...this.props} />
        ) : (
          <Redirect to={{
            pathname: '/account/login',
            state: { from: this.props.location }
          }}/>
        )
      )}/>
    )
  }
}

export default PrivateRoute
