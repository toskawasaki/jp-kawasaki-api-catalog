import React, { Component } from 'react'
import { inject } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('userStore')
class Aside extends Component {

    render() {
        return (
            <aside role="complementary" className="l-complementary">
                <div className="cm cm-rich-text">
                    <h2 className="has-border">Account settings</h2>
                </div>
                <Link className="link-caret" to="/account/login" onClick={() => this.props.userStore.invalidateLogin()}>{this.props.userStore.userProfile.login.loggedIn ? `Logout` : 'Sign in'}</Link>
                <Link to='/account/forgot-password' className="link-caret">Change password</Link>
            </aside>
        )
    }
}

export default Aside
