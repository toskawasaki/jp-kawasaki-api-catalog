import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text, FormError } from 'react-form'
import { Link } from 'react-router-dom'

class VerifyForm extends Component {

    render() {

        return (

            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                validate={values => {
                    const { code } = values
                    return {
                        code: !code ? 'Verification code is required' : undefined,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='verify' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Verify</legend>
                                <div className="ctrl-holder width-l">
                                    <label>Verification code<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='code' className="text" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <button className="cta" type='submit'>Verify</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default VerifyForm
