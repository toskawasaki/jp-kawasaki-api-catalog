import React, { Component } from 'react'
import PropTypes from 'prop-types'

class PageList extends Component {
  render() {
    // const { title, description } = this.props

    return (
        <section>

            {this.props.children}

        </section>
    )
  }
}

PageList.propTypes = {
    children: PropTypes.array.isRequired,
    // title: PropTypes.string.isRequired,
    // description: PropTypes.object.isRequired,
}

export default PageList
