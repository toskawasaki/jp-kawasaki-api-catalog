import React, { Component } from 'react'

import FooterNav from '../../components/FooterNav'
import Svg from '../../components/Svg'

const config = require('../../../config-theme.json')

class Footer extends Component {
  render() {
    return (

        <footer className="global-footer" role="site-footer">
            <div className="l-padding">
                <h1 className="vh">Site footer</h1>

                <div className="footer-logo">
                    <div className="logo">
                        <a href="/">
                            <Svg src={require('../../../static/panasonic-logo.svg')} style={{width:420, height:80}} alt={config.theme.title} />
                        </a>
                    </div>
                    <h2>{config.theme.footer.tagline}</h2>
                </div>

                <FooterNav>
                    {config.theme.footer.links}
                </FooterNav>

                {/*<p>{config.theme.footer.copyright}</p>*/}

            </div>
        </footer>

    )
  }
}

export default Footer
