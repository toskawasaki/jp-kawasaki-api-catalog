import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Svg extends Component {
  render() {
    return (
      <img className={this.props.className} style={this.props.style} alt={this.props.alt} src={this.props.src} />
    )
  }
}

Svg.propTypes = {
  className: PropTypes.object,
  src: PropTypes.string,
  style: PropTypes.object,
  alt: PropTypes.string
}

export default Svg
