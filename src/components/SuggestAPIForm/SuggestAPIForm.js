import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Text, Textarea } from 'react-form'

class SuggestAPIForm extends Component {

    render() {

        return (
            <Form
                onSubmit={(values) => {
                    console.log('Success!', values)
                }}

                validate={values => {
                    const { name, email, apiTitle, apiDescription } = values
                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    return {
                        name: !name ? 'Your name is required' : undefined,
                        email: !re.test(email) ? 'Valid email address is required' : undefined,
                        apiTitle: !apiTitle ? 'API title is required' : undefined,
                        apiDescription: !apiDescription ? 'API description is required' : undefined,
                    }
                }}
            >

                {({submitForm}) => {
                    return (
                        <form id='suggest-api' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Suggest an API</legend>

                                <div className="ctrl-holder width-xl">
                                    <label>Your contact details</label>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Your name<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='name' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>Email<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='email' className="text" />
                                    </div>
                                </div>

                                <div className="ctrl-holder width-xl">
                                    <label>API Request description</label>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit. Nunc elit orci, pellentesque ac faucibus eget, efficitur vel tortor. Pellentesque vitae elit sit amet lacus tincidunt cursus vel quis lorem. Maecenas gravida leo lacus, at suscipit metus tempor id. Nulla malesuada leo sit amet neque scelerisque, id tempus turpis elementum.</p>
                                </div>
                                <div className="ctrl-holder width-l">
                                    <label>API title<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='apiTitle' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-xl">
                                    <label>API description<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Textarea field="apiDescription" className="text" />
                                    </div>
                                </div>

                            </fieldset>
                            <fieldset>
                                <button className="cta" type='submit'>Submit</button>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default SuggestAPIForm
