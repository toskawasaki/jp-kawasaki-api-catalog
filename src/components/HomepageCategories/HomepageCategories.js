import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class HomepageCategories extends Component {

    render() {
        // const { catSlug, categoryTitle, api } = this.props

        return (
            <div className="sl bom-catalogue-category">
                <ul className="sl-list" role="category-items">
                    {this.props.children}
                </ul>
            </div>
        )
    }
}

HomepageCategories.propTypes = {
    // catSlug: PropTypes.string,
    // categoryTitle: PropTypes.string,
    // api: PropTypes.object,
}

export default HomepageCategories
