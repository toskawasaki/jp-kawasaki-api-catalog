import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Switch, Redirect, Route, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

import Key from '../../components/Key'

const credentials = require('../../../credentials.json')
const SUCCESS = 204
const EXPIRY = 401
const FORBIDDEN = 403

@inject('userStore')
@withRouter
@observer
class APIKeys extends Component {

    @action.bound removeKey(data) {


        if (confirm('Are you sure you want to remove your API key?')) {

            const headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': `${this.props.userStore.userProfile.login.token}`,
                'X-Api-Key': credentials.login.key,
            })

            fetch(`${credentials.login.baseUrl}/users/${this.props.userStore.userProfile.username}/applications/${data.apiID}`, {
                method: 'DELETE',
                headers: headers,
            }).then((response) => {

                // console.log(response)

                switch (response.status) {
                    case SUCCESS:
                        this.props.userStore.getKeys()
                        break
                    case EXPIRY:
                        response.json().then((json) => {
                            this.props.userStore.populateErrorMessages(json.message)
                        })
                        break
                    case FORBIDDEN:
                        response.json().then((json) => {
                            this.props.userStore.populateErrorMessages(json.message)
                        })
                        break
                    default:
                        return
                }

            }).catch((error) => {
                this.props.userStore.invalidateLogin()
                this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            })

        }

    }

    render() {

        const { keys } = this.props

        let items = []

        function generateKeys(keys, form_submit) {
            if (keys && keys.length > 0) {
                keys.forEach((key_data) => {
                    items.push(
                        <Key
                            key={key_data.api_key}
                            apiKey={key_data.api_key}
                            apiID={key_data.id}
                            apiTitle={key_data.name}
                            apiDescription={key_data.description}
                            onSubmit={form_submit}
                        />
                    )

                })
            }
            return items
        }

        return (

            <section>

                {generateKeys(keys, this.removeKey)}

            </section>

        )
    }
}

APIKeys.propTypes = {
    keys: PropTypes.object,
}

export default APIKeys
