import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form } from 'react-form'

import CheckboxField from '../../components/Checkbox'

class FilterStatus extends Component {

    render() {

        return (
            <Form
                onChange={this.props.onChange}
            >
                {({submitForm}) => {

                    const statusList = []
                    this.props.children.forEach((status, index) => {
                        statusList.push(
                            <CheckboxField key={index} title={status} event={submitForm} />
                        )
                    })

                    return (
                        <form id='filter-status' className="filter-form">
                            <legend>Filter Status</legend>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    {statusList}
                                </div>
                            </fieldset>
                        </form>

                    )
                }}
            </Form>
        )
    }
}

FilterStatus.propTypes = {
    children: PropTypes.array.isRequired,
}

export default FilterStatus
