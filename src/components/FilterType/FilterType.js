import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form } from 'react-form'

import CheckboxField from '../../components/Checkbox'

class FilterType extends Component {

    render() {

        return (
            <Form
                onChange={this.props.onChange}
            >
                {({submitForm}) => {

                    const typeList = []
                    this.props.children.forEach((type, index) => {
                        typeList.push(
                            <CheckboxField key={index} title={type} event={submitForm} />
                        )
                    })

                    return (
                        <form id='filter-type' className="filter-form">
                            <legend>Filter Type</legend>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    {typeList}
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

FilterType.propTypes = {
    children: PropTypes.array.isRequired,
}

export default FilterType
