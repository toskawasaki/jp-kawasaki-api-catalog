import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ErrorMessages extends Component {

    render() {

        return (

            <section className={this.props.message ? 'error active': 'error'}>
                {this.props.message}
            </section>

        )
    }
}

ErrorMessages.propTypes = {
    message: PropTypes.string,
}

export default ErrorMessages
