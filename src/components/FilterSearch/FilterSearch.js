import React, {Component} from 'react'
import {Form, Text} from 'react-form'

class FilterSearch extends Component {

    render() {

        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
            >
                {({submitForm}) => {

                    return (
                        <form id='filter-search' onSubmit={submitForm}>
                            <fieldset>
                                <div className="ctrl-holder width-full">
                                    <div className="ctrl">
                                        <span className='search-icon'></span>
                                        <Text field='search' className="text" onChange={(e, onChange) => {onChange(submitForm)}}/>
                                        <button className="cta" type='submit'>Search</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default FilterSearch
