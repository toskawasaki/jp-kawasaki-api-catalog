import React, { Component } from 'react'
import { Form, Text, Textarea } from 'react-form'
import { Link } from 'react-router-dom'

class GenerateAPIKey extends Component {

    render() {

        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
                validate={values => {
                    const { apiTitle, apiDescription } = values
                    return {
                        apiTitle: !apiTitle ? 'API title is required' : undefined,
                        apiDescription: !apiDescription ? 'API description is required' : undefined,
                    }
                }}
            >
                {({submitForm}) => {
                    return (
                        <form id='generate-key' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Generate API Key</legend>
                                <div className="ctrl-holder width-l">
                                    <label>Title of your application<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Text field='apiTitle' className="text" />
                                    </div>
                                </div>
                                <div className="ctrl-holder width-xxl">
                                    <label>Description of your application<em>*<span className="vh">Required field</span></em></label>
                                    <div className="ctrl">
                                        <Textarea field="apiDescription" className="text" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <button className="cta" type='submit'>Register application</button>
                                        <Link to='/app' className="cta is-secondary">Cancel</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default GenerateAPIKey
