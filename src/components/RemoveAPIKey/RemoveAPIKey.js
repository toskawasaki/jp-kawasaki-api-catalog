import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form } from 'react-form'
import { Link } from 'react-router-dom'

import CopyToClipboard from 'react-copy-to-clipboard'

class RemoveAPIKey extends Component {

  constructor() {
      super()
      this.state = {
          copied: false,
      }
  }


    render() {

        const apiKey = 'MwDQrigv2C68KbX4FfMKF4CIXxz6vR9y9p2hq0Ev'

        return (
            <Form
                onSubmit={this.props.onSubmit}
                onChange={this.props.onChange}
            >
                {({submitForm}) => {
                    return (
                        <form id='remove-api-key' onSubmit={submitForm}>
                            <fieldset>
                                <legend className="vh">Remove API Key</legend>
                                <div className="ctrl-holder width-xxl">
                                    <div className="ctrl">
                                        <div className="key-block">

                                            <CopyToClipboard text={apiKey} onCopy={() => this.setState({copied: true})}>
                                                <button>Copy API key</button>
                                            </CopyToClipboard>

                                            <input value={apiKey} readOnly='readOnly' />

                                            {this.state.copied ? <div className='checkmark draw'></div> : null}

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div className="ctrl-holder width-l">
                                    <div className="ctrl">
                                        <button className="cta" type='submit'>Remove API key</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    )
                }}
            </Form>
        )
    }
}

export default RemoveAPIKey
