import React, {Component} from 'react'
import {action} from 'mobx'
import {inject, observer} from 'mobx-react'
import {Redirect} from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import ResetPasswordForm from '../../ResetPasswordForm'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const SUCCESS = 200
const FORBIDDEN = 400

@inject('userStore')
@observer
class ResetPassword extends Component {

    @action.bound resetPassword(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'X-Api-Key': credentials.login.key
        })

        const body = {
            "code": data.code,
            "new_password": data.password,
        }

        fetch(`${credentials.login.baseUrl}/users/${data.email}/resetPassword`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(body),
        }).then((response) => {

            // console.log(response.status, typeof response.status)

            switch (response.status) {
                case SUCCESS:
                    this.props.userStore.resetPasswordComplete(true)
                    break
                case FORBIDDEN:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)

        }).catch((error) => {
            // console.log('Request failed', error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })

    }

    componentWillMount() {

        this.props.userStore.resetPasswordInitiated(false)

    }

    componentWillUnmount() {

        this.props.userStore.resetPasswordComplete(false)

    }

    render() {

        if (this.props.userStore.passwordReset) {
            return (<Redirect to='/account/login' />)
        }

        return (

            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Reset password" />

                    {/*<div className="cm cm-rich-text">*/}
                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit.</p>*/}
                    {/*</div>*/}

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <ResetPasswordForm onSubmit = {this.resetPassword} />
                    </Loading>

                </main>

                <Aside />

            </section>



        )
    }
}

export default ResetPassword
