import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import RegisterForm from '../../RegisterForm'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const REGISTER_SUCCESS = 201
const REGISTER_FORBIDDEN = 403
const EXPIRY = 401
const ALREADY_EXISTS = 400

@inject('userStore')
@observer
class Register extends Component {

    @action.bound registerUser(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'X-Api-Key': credentials.login.key,
        })

        const body = {
            "username": data.email,
            "password": data.password,
            "email": data.email
        }

        fetch(`${credentials.login.baseUrl}/users`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(body),
        }).then((response) => {

            // console.log(response)

            switch (response.status) {
                case REGISTER_SUCCESS:
                    response.json().then((json) => {
                        this.props.userStore.userRegistered(json, data)
                    })
                    break
                case REGISTER_FORBIDDEN:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                case ALREADY_EXISTS:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.title)
                    })
                    break
                case EXPIRY:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    this.props.userStore.invalidateLogin()
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)
        }).catch((error) => {
            // console.log('Request failed', error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })
    }

    componentWillUnmount() {

        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {
        if(this.props.userStore.isRegistered){
            return (<Redirect to='/account/verify' />)
        }
        return (
            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Register an account" />

                    {/*<div className="cm cm-rich-text">*/}
                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit.</p>*/}
                    {/*</div>*/}

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <RegisterForm onSubmit={this.registerUser} />
                    </Loading>

                </main>

                <Aside />

            </section>
        )
    }
}

export default Register
