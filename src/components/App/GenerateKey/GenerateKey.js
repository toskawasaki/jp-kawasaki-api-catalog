import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import GenerateAPIKey from '../../GenerateAPIKey'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const SUCCESS = 201
const MAXKEYS = 400
const UNAUTHORIZED = 401
const FORBIDDEN = 403

@inject('userStore')
@observer
class GenerateKey extends Component {

    @action.bound generateKey(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': `${this.props.userStore.userProfile.login.token}`,
            'X-Api-Key': credentials.login.key,
        })

        const body = {
            "name": data.apiTitle,
            "description": data.apiDescription,
            "usage_plan": "basic",
            "version": "1.0"
        }

        fetch(`${credentials.login.baseUrl}/users/${this.props.userStore.userProfile.username}/applications`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(body),
        }).then((response) => {

            // console.log(response.status, typeof response.status)

            switch (response.status) {
                case SUCCESS:
                    this.props.userStore.toggleKeyGen(true)
                    this.props.userStore.toggleLoading(false)
                    break
                case MAXKEYS:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.title)
                    })
                    break
                case UNAUTHORIZED:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                case FORBIDDEN:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)

        }).catch((error) => {
            // console.log('Request failed', error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })

    }

    componentWillUnmount() {

        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {

        if(this.props.userStore.keyGen){
            return (<Redirect to='/account' />)
        }

        return (
            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Register your application and receive an API key" />

                    <div className="cm cm-rich-text">
                        <p>Register your application to receive an API key. Each application is restricted to 5,000 requests a day with a burst request limitation of 100 requests per second.</p>
                    </div>

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <GenerateAPIKey onSubmit={this.generateKey} />
                    </Loading>

                </main>

                <Aside />

            </section>
        )
    }
}

export default GenerateKey
