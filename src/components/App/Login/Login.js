import React, {Component} from 'react'
import {action} from 'mobx'
import {inject, observer} from 'mobx-react'
import {Redirect} from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import LoginForm from '../../LoginForm'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const LOGIN_SUCCESS = 201
const LOGIN_FAILURE = 400
const LOGIN_FORBIDDEN = 403

@inject('userStore')
@observer
class Login extends Component {

    @action.bound performLogin(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Authorization': `Basic ${btoa(`${data.email}:${data.password}`)}`,
            'X-Api-Key': credentials.login.key
        })

        fetch(`${credentials.login.baseUrl}/sessions`, {
            method: 'POST',
            headers: headers,
        }).then((response) => {

            // console.log(response.status, typeof response.status)

            switch (response.status) {
                case LOGIN_SUCCESS:
                    response.json().then((json) => {
                        this.props.userStore.userLoggedIn(json, data)
                        this.props.userStore.toggleLoading(false)
                    })
                    break
                case LOGIN_FAILURE:
                    response.json().then((json) => {
                        // console.log(json.title)
                        this.props.userStore.populateErrorMessages(json.title)
                    })
                    break
                case LOGIN_FORBIDDEN:
                    response.json().then((json) => {
                        // console.log(json.message)
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)
            this.props.userStore.invalidateLogin()
        }).catch((error) => {
            // console.log(error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })
    }

    componentWillUnmount() {

        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {
        if (this.props.userStore.userProfile.login.loggedIn) {
            return (<Redirect to='/account'/>)
        }
        return (
            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Login" />

                    {/*<div className="cm cm-rich-text">*/}
                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit.</p>*/}
                    {/*</div>*/}

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <LoginForm onSubmit={this.performLogin}/>
                    </Loading>

                </main>

                <Aside />

            </section>
        )
    }
}

export default Login
