import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Switch, Redirect, Route, withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import APIKeys from '../../APIKeys'

const credentials = require('../../../../credentials.json')
const SUCCESS = 200
const FAILURE = 404
const EXPIRY = 401

@inject('userStore')
@withRouter
@observer
class Dashboard extends Component {

    @action.bound getKeys() {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': `${this.props.userStore.userProfile.login.token}`,
            'X-Api-Key': credentials.login.key,
        })

        fetch(`${credentials.login.baseUrl}/users/${this.props.userStore.userProfile.username}/applications`, {
            method: 'GET',
            headers: headers,
        }).then((response) => {

            // console.log(response)

            switch (response.status) {
                case SUCCESS:
                    response.json().then((json) => {
                        this.props.userStore.loadKeys(json)
                    })
                    break
                case FAILURE:
                    this.props.userStore.loadKeys()
                    break
                case EXPIRY:
                    response.json().then((json) => {
                        // console.log(json.message)
                    })
                    this.props.userStore.invalidateLogin()
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)


        }).catch((error) => {
            // console.log(error)
            this.props.userStore.invalidateLogin()
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })
    }

    componentDidMount() {

        this.getKeys()

    }

    componentDidUpdate() {

        this.props.userStore.toggleKeyGen(false)
        // this.props.userStore.toggleLoading(false)
        this.props.userStore.resetPasswordComplete(false)

    }

    componentWillUnmount() {

        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {

        if(!this.props.userStore.userProfile.login.loggedIn){
            return (<Redirect to='/account/login' />)
        }

        return (
          <section>

            <main id="main" tabIndex="-1" role="main" className="l-main">

                <section>

                    <PageHeader title="My Account" />

                    <Link to='/account/register-app' className="cta cta-plus">Register an application</Link>
                    <div className="cm cm-rich-text">
                        <p>Register your application to receive an API key. Each application is restricted to 10,000 requests a day with a burst request limitation of 10 requests per second.</p>
                    </div>

                    <Loading active={this.props.userStore.loading}>
                        <APIKeys keys={this.props.userStore.userProfile.apiKeys} />
                    </Loading>

                </section>

            </main>

            <Aside />

          </section>
        )
  }
}

export default Dashboard
