import React, {Component} from 'react'
import {action} from 'mobx'
import {inject, observer} from 'mobx-react'
import {Redirect} from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import ForgotPasswordForm from '../../ForgotPasswordForm'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const SUCCESS = 202
const FORBIDDEN = 403

@inject('userStore')
@observer
class ForgotPassword extends Component {

    @action.bound forgotPassword(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'X-Api-Key': credentials.login.key
        })

        fetch(`${credentials.login.baseUrl}/users/${data.email}/sendPasswordReset`, {
            method: 'POST',
            headers: headers,
        }).then((response) => {

            // console.log(response.status, typeof response.status)

            switch (response.status) {
                case SUCCESS:
                    this.props.userStore.resetPasswordInitiated(true)
                    break
                case FORBIDDEN:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)

        }).catch((error) => {
            // console.log('Request failed', error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })

    }

    componentWillUnmount() {

        this.props.userStore.resetPasswordInitiated(false)
        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {

        if (this.props.userStore.passwordForgot) {
            return (<Redirect to='/account/reset-password' />)
        }

        return (

            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Forgot password" />

                    <div className="cm cm-rich-text">
                        <p>Please provide additional information to aid in the recovery process. Enter the email associated with your account.</p>
                    </div>

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <ForgotPasswordForm onSubmit = {this.forgotPassword} />
                    </Loading>

                </main>

                <Aside />

            </section>



        )
    }
}

export default ForgotPassword
