import { action, computed, observable } from 'mobx'
import { persist } from 'mobx-persist'

const credentials = require('../../../../credentials.json')
const SUCCESS = 200
const FAILURE = 404
const EXPIRY = 401

const defaults = {
  details: {
    username: '',
    first: '', // this is never being submitted to the API
    last: '', // this is never being submitted to the API
    email: ''
  }
}

let profile = {
    fullname: '',
    username: '',
    email: '',
    register: {
        confirmed: false,
        delivery_destination: null,
    },
    login: {
        loggedIn: false,
        token: null,
    },
    apiKeys: null,
}

export default class UserStore {

  @persist @observable keyGen = false
  @persist @observable loading = false
  @persist @observable passwordForgot = false
  @persist @observable passwordReset = false
  @persist @observable errorMessage = null
  @persist('object') @observable details = {...defaults.details}
  @persist('object') @observable userProfile = {...profile}

  @computed get isLoggedIn() {
    return this.userProfile.login.loggedIn
  }

  @computed get isConfirmed() {
    return this.userProfile.register.confirmed
  }

  @computed get isRegistered() {
      return typeof this.userProfile.register.delivery_destination === 'string'
  }

  @action invalidateLogin() {

      this.userProfile.login.loggedIn = false
      this.userProfile.login.token = null
      this.details = {...defaults.details}

      this.keyGen = false
      this.userProfile = {...profile}

  }

  @action userLoggedIn(res, data) {

    // this.userProfile.fullname = data.name
    this.userProfile.username = data.email
    this.userProfile.email = data.email
    this.userProfile.login.loggedIn = true
    this.userProfile.login.token = res.id_token

  }


  @action toggleKeyGen(toggle) {
    this.keyGen = toggle
  }


    @action toggleLoading(toggle) {
        this.loading = toggle
    }



  @action loadKeys(res) {

      // console.log(res)
      this.userProfile.apiKeys = []

      if (res) {
          for (let key of res) {
              this.userProfile.apiKeys.push(key)
          }
      }

  }

  @action userRegistered(res, data) {

      this.userProfile.username = data.email
      this.userProfile.email = data.email
      this.userProfile.register.confirmed = res.confirmed
      this.userProfile.register.delivery_destination = res.delivery_details.destination

  }

  @action userVerified(confirmed) {
      this.userProfile.register.confirmed = confirmed
  }


  @action resetPasswordInitiated(confirmed) {
      this.passwordForgot = confirmed
  }

    @action resetPasswordComplete(confirmed) {
        this.passwordReset = confirmed
    }

    @action populateErrorMessages(msg) {
        this.errorMessage = msg
    }




    @action getKeys() {

        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': `${this.userProfile.login.token}`,
            'X-Api-Key': credentials.login.key,
        })

        fetch(`${credentials.login.baseUrl}/users/${this.userProfile.username}/applications`, {
            method: 'GET',
            headers: headers,
        }).then((response) => {
            // handle success / failure here
            // console.log(response)

            switch (response.status) {
                case SUCCESS:
                    response.json().then((json) => {
                        this.loadKeys(json)
                    })
                    break
                case FAILURE:
                    this.loadKeys()
                    break
                case EXPIRY:
                    response.json().then((json) => {
                        // console.log(json.message)
                    })
                    this.invalidateLogin()
                    break
                default:
                    return
            }

        }).catch((error) => {
            // console.log(error)
            this.invalidateLogin()
            this.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
        })
    }


}
