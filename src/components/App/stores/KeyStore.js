import { observable } from 'mobx'

export default class KeyStore {
  @observable keys = []
}
