import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router'

import PageHeader from '../../PageHeader'
import Aside from '../../Aside'
import Loading from '../../Loading'
import VerifyForm from '../../VerifyForm'
import ErrorMessages from '../../ErrorMessages'

const credentials = require('../../../../credentials.json')
const VERIFY_SUCCESS = 204
const VERIFY_FORBIDDEN = 403

@inject('userStore')
@observer
class Verify extends Component {

    @action.bound verifyUser(data) {

        this.props.userStore.toggleLoading(true)

        const headers = new Headers({
            'Content-Type': 'application/json',
            'X-Api-Key': credentials.login.key,
        })

        fetch(`${credentials.login.baseUrl}/users/${this.props.userStore.userProfile.username}/confirmEmail?code=${data.code}`, {
            method: 'GET',
            headers: headers,
        }).then((response) => {

            switch (response.status) {
                case VERIFY_SUCCESS:
                    this.props.userStore.userVerified(true)
                    break
                case VERIFY_FORBIDDEN:
                    response.json().then((json) => {
                        this.props.userStore.populateErrorMessages(json.message)
                    })
                    break
                default:
                    this.props.userStore.toggleLoading(false)
                    return
            }

            this.props.userStore.toggleLoading(false)

        }).catch((error) => {
            // console.log('Request failed', error)
            this.props.userStore.populateErrorMessages('There has been an error connecting to the portal service. Please try logging in again.')
            this.props.userStore.toggleLoading(false)
        })
    }

    componentWillUnmount() {

        this.props.userStore.toggleLoading(false)
        this.props.userStore.populateErrorMessages(null)

    }

    render() {
        if(!this.props.userStore.isRegistered){
            return (<Redirect to='/account/register' />)
        }
        if(this.props.userStore.isConfirmed){
            return (<Redirect to='/account/login' />)
        }
        return (
            <section>

                <main id="main" tabIndex="-1" role="main" className="l-main">

                    <PageHeader title="Verify your account" />

                    <div className="cm cm-rich-text">
                        <p>We have sent a verification code to your email address:</p>
                        <p>{this.props.userStore.userProfile.register.delivery_destination}</p>
                    </div>

                    <ErrorMessages message={this.props.userStore.errorMessage} />

                    <Loading active={this.props.userStore.loading}>
                        <VerifyForm onSubmit={this.verifyUser} />
                    </Loading>

                </main>

                <Aside />

            </section>
        )
    }
}

export default Verify
