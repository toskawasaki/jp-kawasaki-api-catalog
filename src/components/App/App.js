import React, { Component } from 'react'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import DevTools from 'mobx-react-devtools'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'

import Dashboard from './Dashboard/Dashboard'
import GenerateKey from './GenerateKey/GenerateKey'
import Login from './Login/Login'
import Register from './Register/Register'
import Verify from './Verify/Verify'
import ForgotPassword from './ForgotPassword/ForgotPassword'
import ResetPassword from './ResetPassword/ResetPassword'
import PrivateRoute from '../PrivateRoute'

// @inject('keyStore')
@inject('userStore')
@observer
class App extends Component {
  render() {
    // only render if we're actually in a browser
    if (typeof window !== 'undefined') {
      return (
        <section>
          <BrowserRouter>
            <Switch>
              <Route path="/account/reset-password" component={ResetPassword} />
              <Route path="/account/forgot-password" component={ForgotPassword} />
              <Route path="/account/register" component={Register} />
              <Route path="/account/verify" component={Verify} />
              <Route path="/account/login" component={Login} />
              <PrivateRoute path="/account/register-app" component={GenerateKey} />
              <PrivateRoute path="/account" component={Dashboard} />
            </Switch>
          </BrowserRouter>
          {process.env.NODE_ENV !== `production` ? <DevTools /> : null}
        </section>
      )
    }
    // don't render anything if being generated statically by gatsby.
    return null
  }
}

export default App
