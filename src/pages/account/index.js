import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'

import {action, useStrict} from 'mobx'
import {observer, Provider} from 'mobx-react'
import {KeyStore, UserStore} from '../../components/App/stores/'
import {create} from 'mobx-persist'

import App from '../../components/App/App'

// import Flag from '../../components/Flag'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Navigation from '../../components/Navigation'

const config = require('../../../config-theme.json')

// enforce actions for mobx
useStrict(true)

let app = ''

// wrap this code to stop it breaking gatsby's static html generation.
// @todo consider moving this into the App.
if ((typeof window !== 'undefined')) {
    const stores = {
        keyStore: new KeyStore(),
        userStore: new UserStore()
    }

    const hydrate = create()
    hydrate('user', stores.userStore).then((args) => {
        console.log('hydrated!', args)
    })

    app = (
        <Provider {...stores}>
            <App />
        </Provider>
    )
}

class AppIndex extends Component {

    constructor() {
        super()
    }

    render() {
        return (
            <div className="page-wrap-outer">
                <div className="page-wrap">

                    {/*<Flag text="Experimental" />*/}

                    <Header
                        title={config.theme.title}
                        link="/"
                    />

                    <Navigation>

                        {config.theme.navigation}

                    </Navigation>

                    <div className="l-layout cf l-three-column">
                        <div className="l-content-container cf l-padding">
                            <article className="l-content-column">
                                {app}
                            </article>
                        </div>
                    </div>

                    <Footer />
                </div>
            </div>
        )
    }
}

AppIndex.propTypes = {}

export default AppIndex
