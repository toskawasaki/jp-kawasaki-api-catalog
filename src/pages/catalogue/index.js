import React, { Component } from 'react'
// import { StickyContainer, Sticky } from 'react-sticky'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'

// import Flag from '../../components/Flag'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Navigation from '../../components/Navigation'
// import SideMenu from '../../components/SideMenu'
// import StickySideMenu from '../../components/StickySideMenu'
import FilterDomains from '../../components/FilterDomains'
import FilterStatus from '../../components/FilterStatus'
import FilterSearch from '../../components/FilterSearch'
import FilterType from '../../components/FilterType'
import Category from '../../components/Category'
import PageList from '../../components/PageList'

const config = require('../../../config-theme.json')

class Catalogue extends React.Component {

    constructor(props) {
        super()
        const {categories, status, types} = props.data.allSwagger
        this.state = {
            initialDomains: categories,
            initialStatus: status,
            initialTypes: types,
            selectedDomains: [],
            selectedStatus: [],
            selectedTypes: [],
            searchTerm: ''
        }
    }

    filterDomains(data) {

        let initialItems = this.state.initialDomains
        let selectedItems = []

        // add selected items to an array (value = true)
        Object.keys(data.values).map(function(key) {
            (data.values[key]) ? selectedItems.push(key) : null
        })

        // if not items are selected then set all items as selected
        if (selectedItems.length === 0) {
            selectedItems = initialItems
        }

        this.setState({selectedDomains: selectedItems})

    }

    filterStatus(data) {

        let initialItems = this.state.initialStatus
        let selectedItems = []

        // add selected items to an array (value = true)
        Object.keys(data.values).map(function(key) {
            (data.values[key]) ? selectedItems.push(key) : null
        })

        // if not items are selected then set all items as selected
        if (selectedItems.length === 0) {
            selectedItems = initialItems
        }

        this.setState({selectedStatus: selectedItems})

    }

    filterType(data) {

        let initialItems = this.state.initialTypes
        let selectedItems = []

        // add selected items to an array (value = true)
        Object.keys(data.values).map(function(key) {
            (data.values[key]) ? selectedItems.push(key) : null
        })

        // if not items are selected then set all items as selected
        if (selectedItems.length === 0) {
            selectedItems = initialItems
        }

        this.setState({selectedTypes: selectedItems})

    }

    filterSearch(data) {

        this.setState({searchTerm: data.search})

    }

    filterSearchChange(data) {

        this.setState({searchTerm: data.values.search})

    }

    render() {

        const siteMetadata = this.props.data.site.siteMetadata

        const {items} = this.props.data.allSwagger

        const categoryList = []
        const menuItems = []

        // apply filter and search selections to list of endpoints
        this.state.initialDomains.forEach((cat, index) => {
            if (this.state.selectedDomains.indexOf(cat) > -1) {

                let statusNodes = []
                items[index].edges.forEach((value) => {
                    if (this.state.selectedStatus.includes(value.node.catalogue.status)) {
                        statusNodes.push(value)
                    }

                })

                let typeNodes = []
                statusNodes.forEach((value) => {
                    if (this.state.selectedTypes.includes(value.node.catalogue.type)) {
                        typeNodes.push(value)
                    }
                })

                let searchNodes = []
                if (this.state.searchTerm) {
                    typeNodes.forEach((value) => {
                        if (value.node.spec.info.title.toLowerCase().includes(this.state.searchTerm.toLowerCase()) ||
                            value.node.spec.info.description.toLowerCase().includes(this.state.searchTerm.toLowerCase())) {
                            searchNodes.push(value)
                        }
                    })
                } else {
                    searchNodes = typeNodes
                }

                if (searchNodes.length > 0) {
                    categoryList.push(
                        <Category key={index} title={cat} nodes={searchNodes}/>
                    )
                }
            }
        })


        // render sidebar menu items
        this.state.initialDomains.forEach((cat, index) => {
            menuItems.push(
                <ul key={cat}>
                    <li><Link style={{'textTransform': 'capitalize'}} to={`/catalogue#${cat}`}>{cat}</Link></li>
                </ul>
            )
        })

        return (

          <div className="page-wrap">

              {/*<Flag text="Experimental" />*/}

              <Header
                  title={config.theme.title}
                  link="/"
              />

              <Navigation>

                  {config.theme.navigation}

              </Navigation>

              {/*<StickyContainer>*/}
                  <div className="l-layout cf l-two-column-right">
                      <div className="l-content-container cf l-padding">

                          {/*<StickySideMenu>*/}

                              <aside id="aside" tabIndex="-1" role="complementary" className="l-aside">

                                  <nav className="nav-aside" role="navigation">
                                      <h6>Take me to</h6>
                                      <ul>
                                          <li>
                                              <Link to='/catalogue'>Data Catalogue</Link>

                                              {menuItems}

                                              <ul key="suggest">
                                                  <li><Link style={{'textTransform': 'capitalize'}} to={`/suggest-endpoint`}>Suggest endpoint</Link></li>
                                              </ul>
                                          </li>
                                      </ul>
                                  </nav>

                                  <FilterDomains onChange={this.filterDomains.bind(this)}>
                                      {this.state.initialDomains}
                                  </FilterDomains>

                                  <FilterStatus onChange={this.filterStatus.bind(this)}>
                                      {this.state.initialStatus}
                                  </FilterStatus>

                                  <FilterType onChange={this.filterType.bind(this)}>
                                      {this.state.initialTypes}
                                  </FilterType>

                              </aside>

                          {/*</StickySideMenu>*/}

                          <article className="l-content-column">
                              <main id="main" tabIndex="-1" role="main" className="l-main">

                                  <Helmet title={siteMetadata.title}/>

                                  <header className="content-header">
                                      <h1>Data Catalogue</h1>
                                  </header>

                                  <div className="cm cm-rich-text">
                                      <div><p>Panasonic APIs provide simple and clear programmatic access to trusted, reliable and responsive data. To access the APIs you need to sign up for a user account and register your application to receive an API key.</p><p>Please read the <Link to='/how-to-get-started'>How to get started</Link> guide for instructions.</p></div>
                                  </div>

                                  <FilterSearch onSubmit={this.filterSearch.bind(this)} onChange={this.filterSearchChange.bind(this)} />

                                  <PageList>

                                      {categoryList}

                                  </PageList>

                              </main>
                          </article>

                      </div>
                  </div>
              {/*</StickyContainer>*/}

              <Footer />

          </div>

      )
  }
}

export default Catalogue

export const pageQuery = graphql`
query CatalogueQuery {
  site {
    siteMetadata {
      title
    }
  }
  allSwagger {
    categories: distinct(field:catalogue___category)
    status: distinct(field:catalogue___status)
    types: distinct(field:catalogue___type)
    items: group(field:catalogue___category) {
      edges {
        node {
          slug
          catalogue {
            category
            status
            type
          }
          spec {
            info {
              title
              description
              version
            }
            host
            basePath
          }
        }
      }
    }
  }
}
`
