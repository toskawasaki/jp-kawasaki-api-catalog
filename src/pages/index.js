import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

// import Flag from '../components/Flag'
import HeaderHome from '../components/HeaderHome'
import Footer from '../components/Footer'
import NavigationIndex from '../components/NavigationIndex'
import PageHeader from '../components/PageHeader'
import CTA from '../components/CTA'
import HomepageCategories from '../components/HomepageCategories'
import CategoryPanel from '../components/CategoryPanel'

import './index.scss'

const config = require('../../config-theme.json')

class PageIndex extends Component {
  render() {

    return (
        <div className="page-wrap-outer">
            <div className="page-wrap is-home">

                {/*<Flag text="Experimental" />*/}

                <HeaderHome
                    title={config.theme.title}
                    slogan={config.theme.slogan}
                    link="/"
                />

                <NavigationIndex>

                    {config.theme.navigation}

                </NavigationIndex>

                <div className="l-layout cf l-one-column">

                    <div className="l-content-container cf l-padding">

                        <article className="l-content-column">
                            <main id="main" tabIndex="-1" role="main" className="l-main">

                                <Helmet title="Panasonic Digital Platform" />

                                <section className="data-catalogue-home">

                                    <PageHeader title="Explore our data catalogue by selecting a category below" />

                                    <HomepageCategories>
                                        <CategoryPanel title="Data Catalogue"
                                                       link="/catalogue"
                                                       icon="data-catalogue"/>
                                        <li className="sl-item">
                                            <a className="api-tile" href="http://logging.private.prod.nabpd.local/_plugin/kibana">
                                                <h3 className="icon analytics">Analytics</h3>
                                            </a>
                                        </li>
                                        <li className="sl-item">
      										<a className="api-tile" href="https://10.0.28.20:8443/console/project/obplatform/overview">
            									<h3 className="icon platforms">Platforms</h3>
      										</a>
										</li>
                                      <li className="sl-item">
                                         <a className="api-tile" href="https://panasonic-iot.atlassian.net/wiki/spaces/PF/overview">
                                            <h3 className="icon data-catalogue">Wiki</h3>
                                            </a>
                                      </li>
                                    </HomepageCategories>

                                    <CTA title="How to get started"
                                         text="Explore the data catalogue, register for usage, suggest a new integration. Click on Get Started to begin your journey."
                                         buttonLink="/how-to-get-started"
                                         buttonText="Get started"
                                         hasBorder={true}
                                         isCentered={true}
                                    />

                                </section>

                            </main>
                        </article>

                    </div>

                </div>

                <Footer />

            </div>
        </div>
    )

  }
}

PageIndex.propTypes = {
  data: PropTypes.object,
}

export default PageIndex
