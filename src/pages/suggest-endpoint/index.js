import React, { Component } from 'react'
import { StickyContainer, Sticky } from 'react-sticky'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

// import Flag from '../../components/Flag'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Navigation from '../../components/Navigation'
// import StickySideMenu from '../../components/StickySideMenu'
import PageHeader from '../../components/PageHeader'
import SuggestAPIForm from '../../components/SuggestAPIForm'

const config = require('../../../config-theme.json')

class SuggestAPI extends Component {
    render() {
        const pageLinks = []

        return (
            <div className="page-wrap-outer">
                <div className="page-wrap">

                    {/*<Flag text="Experimental" />*/}

                    <Header
                        title={config.theme.title}
                        link="/"
                    />

                    <Navigation>

                        {config.theme.navigation}

                    </Navigation>

                    {/*<StickyContainer>*/}
                        <div className="l-layout cf l-three-column">

                            <div className="l-content-container cf l-padding">

                                {/*<StickySideMenu />*/}

                                <article className="l-content-column">
                                    <main id="main" tabIndex="-1" role="main" className="l-main">

                                        {/*<Helmet title={`${post.frontmatter.title} | ${siteTitle}`}/>*/}

                                        <PageHeader title="Suggest an endpoint" />

                                        {/*<div className='cm cm-rich-text' dangerouslySetInnerHTML={{ __html: post.html }} />*/}
                                        <div className="cm cm-rich-text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit. Nunc elit orci, pellentesque ac faucibus eget, efficitur vel tortor. Pellentesque vitae elit sit amet lacus tincidunt cursus vel quis lorem. Maecenas gravida leo lacus, at suscipit metus tempor id. Nulla malesuada leo sit amet neque scelerisque, id tempus turpis elementum.</p>
                                        </div>

                                        <SuggestAPIForm />

                                    </main>
                                </article>

                            </div>

                        </div>
                    {/*</StickyContainer>*/}

                    <Footer />

                </div>
            </div>
        )

    }
}

// SuggestAPI.propTypes = {
//     data: PropTypes.object,
// }

export default SuggestAPI
