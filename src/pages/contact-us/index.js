import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

// import Flag from '../../components/Flag'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Navigation from '../../components/Navigation'
import SideMenu from '../../components/SideMenu'
import PageHeader from '../../components/PageHeader'
import ContactForm from '../../components/ContactForm'

const config = require('../../../config-theme.json')

class ContactUs extends React.Component {
    render() {

        return (
            <div className="page-wrap-outer">
                <div className="page-wrap">

                    {/*<Flag text="Experimental" />*/}

                    <Header
                        title={config.theme.title}
                        link="/"
                    />

                    <Navigation>

                        {config.theme.navigation}

                    </Navigation>

                    <div className="l-layout cf l-three-column">

                        <div className="l-content-container cf l-padding">

                            <article className="l-content-column">
                                <main id="main" tabIndex="-1" role="main" className="l-main">

                                    {/*<Helmet title={`${post.frontmatter.title} | ${siteTitle}`}/>*/}

                                    <PageHeader title="Contact us" />

                                    <div className="cm cm-rich-text">
                                        <p>We welcome your feedback – complaints, compliments and suggestions – about the services we provide and how we deliver them. This feedback helps us improve the way we do things.</p>
                                    </div>

                                    <ContactForm />

                                </main>
                            </article>

                            {/*<SideMenu title="Take me to" />*/}

                        </div>

                    </div>

                    <Footer title="API Catalogue" />

                </div>
            </div>
        )

    }
}

// ContactUs.propTypes = {
//     data: PropTypes.object,
// }

export default ContactUs
