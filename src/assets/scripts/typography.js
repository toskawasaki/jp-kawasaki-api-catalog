import Typography from 'typography'
import theme from 'typography-theme-fairy-gates'

const googleFonts = [
    {
        name: 'Open Sans',
        styles: [
            '300',
            // '300i',
            '400',
            // '400i',
            '600',
            // '600i',
        ],
    },
]

const typography = new Typography({
    ...theme,
    googleFonts: googleFonts,
})

// Hot reload typography in development.
if (process.env.NODE_ENV !== 'production') {
    typography.injectStyles()
}

export default typography
