import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Wrapper extends Component {
  render() {

    // const { location, children, route } = this.props

    return (

        <div className="page-wrap-outer">

            {this.props.children()}

        </div>

    )
  }
}

Wrapper.propTypes = {
    children: PropTypes.func,
    // location: PropTypes.object,
    // route: PropTypes.object,
}

export default Wrapper
