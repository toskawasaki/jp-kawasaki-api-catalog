import React, {Component} from 'react'
// import { StickyContainer } from 'react-sticky'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

// import Flag from '../components/Flag'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Navigation from '../components/Navigation'
// import StickySideMenu from '../components/StickySideMenu'
import PageHeader from '../components/PageHeader'
import Button from '../components/Button'
import SwaggerUI from '../components/SwaggerUI'

import HelperClass from '../utils/helper'

const config = require('../../config-theme.json')


class CataloguePageRoute extends Component {

    constructor() {
        super()
        this.state = {
            swaggerUI: '',
        }
    }

    componentWillUnmount() {
        clearTimeout()
    }

    loadStatus() {

        if (this.props.data.swagger.catalogue) {
            return (
                <dl>
                    <dt>Status:</dt>
                    <dd>{this.props.data.swagger.catalogue.status}</dd>
                </dl>
            )
        }

    }

    render() {

        const { swagger } = this.props.data
        const siteTitle = this.props.data.site.siteMetadata.title

        if ((typeof window !== 'undefined')) {
            setTimeout(() => {
                this.setState({swaggerUI: (<SwaggerUI spec={JSON.parse(swagger.swagger)}/>)})
            }, 300)
        }

        return (
            <div className="page-wrap">

                {/*<Flag text="Experimental" />*/}

                <Header
                    title={config.theme.title}
                    link="/"
                />

                <Navigation>

                    {config.theme.navigation}

                </Navigation>

                {/*<StickyContainer>*/}
                <div className="l-layout cf l-two-column">
                    <div className="l-content-container cf l-padding">

                        {/*<StickySideMenu />*/}

                        <article className="l-content-column">
                            <main id="main" tabIndex="-1" role="main" className="l-main">

                                <Helmet title={`${swagger.spec.info.title} | ${siteTitle}`}/>

                                <section className='bom-catalogue-detail'>

                                    <PageHeader title={swagger.spec.info.title}/>

                                    <div className="cm cm-rich-text">

                                        <p>{swagger.spec.info.description}</p>

                                        <Button text="How to get started"
                                                link="/how-to-get-started"
                                                isSecondary={true}
                                        />

                                        <h2>API Details</h2>
                                        <div className="api-details">
                                            { this.loadStatus() }
                                            <dl>
                                                <dt>Service Name:</dt>
                                                <dd>{swagger.spec.info.title}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Service Version:</dt>
                                                <dd>{swagger.spec.info.version}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Base URL:</dt>
                                                <dd>https://{swagger.spec.host}{swagger.spec.basePath}</dd>
                                            </dl>
                                        </div>

                                        {/*<h1>Lorem ipsum dolor sit amet</h1>*/}
                                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci leo, dictum sed metus at, sodales aliquam velit. Nunc elit orci, pellentesque ac faucibus eget, efficitur vel tortor. Pellentesque vitae elit sit amet lacus tincidunt cursus vel quis lorem. Maecenas gravida leo lacus, at suscipit metus tempor id. Nulla malesuada leo sit amet neque scelerisque, id tempus turpis elementum.</p>*/}

                                        <div className='swagger-wrapper'>
                                            { this.state.swaggerUI }
                                            <span className='loader'/>
                                        </div>

                                        <Button text="Back to data catalogue"
                                                link="/catalogue"
                                        />

                                    </div>

                                </section>

                            </main>
                        </article>

                    </div>
                </div>
                {/*</StickyContainer>*/}

                <Footer />

            </div>

        )
    }
}

CataloguePageRoute.propTypes = {
    data: PropTypes.object.isRequired,
}

export default CataloguePageRoute

export const pageQuery = graphql`
query CataloguePageBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    swagger(slug: { eq: $slug }) {
        slug
        swagger
        catalogue {
          category
          status
        }
        spec {
          info {
            title
            description
            version
          }
          host
          basePath
      }
    }
}
`
