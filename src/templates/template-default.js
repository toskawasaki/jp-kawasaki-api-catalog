import React, { Component } from 'react'
import { StickyContainer } from 'react-sticky'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

// import Flag from '../components/Flag'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Navigation from '../components/Navigation'
import StickySideMenu from '../components/StickySideMenu'

const config = require('../../config-theme.json')

class DefaultRoute extends Component {
  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title

    return (

        <div className="page-wrap">

            {/*<Flag text="Experimental" />*/}

            <Header
                title={config.theme.title}
                link="/"
            />

            <Navigation>

                {config.theme.navigation}

            </Navigation>

            <StickyContainer>
                <div className="l-layout cf l-two-column-right">

                    <div className="l-content-container cf l-padding">

                        <StickySideMenu />

                        <article className="l-content-column">
                            <main id="main" tabIndex="-1" role="main" className="l-main">

                                <Helmet title={`${post.frontmatter.title} | ${siteTitle}`}/>

                                <header className='content-header'>
                                    <h1>{post.frontmatter.title}</h1>
                                </header>

                                <div className='cm cm-rich-text' dangerouslySetInnerHTML={{ __html: post.html }} />

                            </main>
                        </article>

                    </div>

                </div>
            </StickyContainer>

            <Footer />

        </div>

    )
  }
}

DefaultRoute.propTypes = {
  data: PropTypes.object.isRequired,
}

export default DefaultRoute

export const pageQuery = graphql`
  query DefaultPageBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug }}) {
      html
      fields {
        slug
      }
      frontmatter {
        title
      }
    }
  }
`
