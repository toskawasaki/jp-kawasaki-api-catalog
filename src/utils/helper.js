
class ClassModifiersHelper {

    static setIconClass(classes, icon) {
        switch (icon) {
            case 'data-catalogue':
                classes = classes.concat(' ', icon)
                break
            case 'api-portal':
                classes = classes.concat(' ', icon)
                break
            case 'locations':
                classes = classes.concat(' custom-icon-rain')
                break
            case 'warnings':
                classes = classes.concat(' custom-icon-storm')
                break
            default:
                classes = classes.concat(' ', icon)
        }
        return classes
    }

}

export default ClassModifiersHelper

