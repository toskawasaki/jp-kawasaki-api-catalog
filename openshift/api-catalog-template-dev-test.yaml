apiVersion: v1
kind: Template
labels:
  template: belong-ui
metadata:
  annotations:
    description: Creates the Belong UI Openshift resources
    iconClass: icon-bolt
    tags: belong-ui
  name: belong-ui
message: Template will create BuildConfig, DeploymentConfig, Service and Route.
parameters:
- description: Route's hostname
  displayName: Route's hostname
  value: origin-dev.mobile.belongtest.com.au
  name: ROUTE_HOSTNAME
  required: true
- description: Git repository
  displayName: Git repository
  value: git@bitbucket.org:belongadmin/belong-ui.git
  name: GIT_REPO
  required: true
- description: Git repository branch
  displayName: Git repository branch
  value: develop
  name: GIT_BRANCH
  required: true
- description: Source Secret
  displayName: Source Secret
  value: dpe-belong-non-prod-dk-secret
  name: SOURCE_SECRET
- description: Builder Image Repo
  displayName: Builder Image Repo
  value: 826875883363.dkr.ecr.ap-southeast-2.amazonaws.com/mobile/base/mobile-belong-ui-builder-image:latest
  name: BUILDER_IMAGE
- description: Application Image Repo
  displayName: Application Image Repo
  value: 826875883363.dkr.ecr.ap-southeast-2.amazonaws.com/mobile/apps/belong-ui:latest
  name: APP_IMAGE
  required: true
- description: Image Push/Pull Secret
  displayName: Image Push/Pull Secret
  value: ecr-registry
  name: PUSH_PULL_SECRET
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: belong-ui
    namespace: development
    labels:
      build: belong-ui
    annotations:
  spec:
    triggers:
      - type: ConfigChange
    runPolicy: SerialLatestOnly
    source:
      type: Git
      git:
        uri: ${GIT_REPO}
        ref: ${GIT_BRANCH}
      sourceSecret:
        name: ${SOURCE_SECRET}
    strategy:
      type: Source
      sourceStrategy:
        from:
          kind: DockerImage
          name: ${BUILDER_IMAGE}
        pullSecret:
          name: ${PUSH_PULL_SECRET}
        incremental: false
        forcePull: true
    output:
      to:
        kind: DockerImage
        name: ${APP_IMAGE}
      pushSecret:
        name: ${PUSH_PULL_SECRET}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: belong-ui
    labels:
      app: belong-ui
    annotations:
  spec:
    strategy:
      type: Rolling
      rollingParams:
        updatePeriodSeconds: 1
        intervalSeconds: 1
        timeoutSeconds: 600
        maxUnavailable: 25%
        maxSurge: 25%
      resources: {}
    triggers:
      - type: ConfigChange
    replicas: 1
    test: false
    selector:
      app: belong-ui
      deploymentconfig: belong-ui
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: belong-ui
          deploymentconfig: belong-ui
        annotations:
      spec:
        containers:
          - name: mobile-activation-api-v1
            image: >-
              ${APP_IMAGE}
            ports:
              - name: http
                containerPort: 8080
                protocol: TCP
            env:
              - name: PORT
                value: '8080'
              - name: NODE_ENV
                value: 'production'
            resources: {}
            livenessProbe:
              httpGet:
                path: /
                port: 8080
                scheme: HTTP
              initialDelaySeconds: 10
              timeoutSeconds: 5
              periodSeconds: 10
              successThreshold: 1
              failureThreshold: 3
            readinessProbe:
              httpGet:
                path: /
                port: 8080
                scheme: HTTP
              initialDelaySeconds: 30
              timeoutSeconds: 5
              periodSeconds: 10
              successThreshold: 1
              failureThreshold: 3
            terminationMessagePath: /dev/termination-log
            imagePullPolicy: Always
        restartPolicy: Always
        terminationGracePeriodSeconds: 30
        dnsPolicy: ClusterFirst
        securityContext: {}
- apiVersion: v1
  kind: Service
  metadata:
    name: belong-ui
    labels:
      app: belong-ui
  spec:
    ports:
      - protocol: TCP
        port: 8080
        targetPort: 8080
    selector:
      deploymentconfig: belong-ui
    type: ClusterIP
    sessionAffinity: None
- kind: Route
  metadata:
    name: belong-ui
    labels:
      app: belong-ui
    annotations:
      belong.com.au/apisecurityheader: testapisecuritykey
  spec:
    host: ${ROUTE_HOSTNAME}
    to:
      kind: Service
      name: belong-ui
      weight: 100
    wildcardPolicy: None