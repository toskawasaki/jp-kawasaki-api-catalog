
node {

    stage('Checkout') {
        checkout scm
    }

    stage('Build Image') {
         println '------- OPENSHIFT DEV BUILD START -------'

        sh """
            set +x
            oc whoami
            oc project obplatform
            oc start-build api-catalog-static -n obplatform --follow=true --wait=true --from-dir="public/"
        """
        println '------- OPENSHIFT DEV BUILD END  -------'
    }

    stage('Deploy Image') {
         println '------- OPENSHIFT DEV BUILD START -------'

        sh """
            set +x
            oc whoami
            oc project obplatform
            oc deploy api-catalog-static -n obplatform --latest --follow
        """
        println '------- OPENSHIFT DEV BUILD END  -------'
    }
}