module.exports = {
  siteMetadata: {
    title: 'Panasonic Digital Platform',
    author: 'James Redman',
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages/`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 690,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
          },
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-transformer-swagger`,
    `gatsby-plugin-postcss-sass`,
    `gatsby-plugin-sharp`,
    {
    resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Panasonic Digital Platform`,
        short_name: `DBP`,
        icons: [{
          src: `/static/panasonic-logo.svg`,
          sizes: `420x80`,
          type: `image/svg`,
        },
        ],
        start_url: `/`,
        background_color: `white`,
        theme_color: `white`,
        display: `minimal-ui`,
      },
    },
    `gatsby-plugin-offline`,
      {
          resolve: `gatsby-plugin-google-tagmanager`,
          options: {
              id: '',
          },
      },
  ],
}
